import React from 'react';
import { shallow } from 'enzyme';
import ExampleComponent from './';

describe('ExampleComponent', () => {
  it('is truthy', () => {
    expect(ExampleComponent).toBeTruthy();
  });

  it('renders empty text', () => {
    expect(shallow(<ExampleComponent />)).toBeTruthy();
  });

  it('renders text', () => {
    expect(shallow(<ExampleComponent text="Hello" />)).toBeTruthy();
  });
});
