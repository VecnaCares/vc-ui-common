module.exports = {
  components: 'src/**/*.{js,jsx,ts,tsx}',
  ignore: [
    'src/setupTests.js',
    'src/**/*.spec.js',
    'src/**/*test.js',
  ],
};
